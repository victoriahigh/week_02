package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextAnalyzerImplTest {

    @Test
    public void analyze() {
        TextAnalyzer analyzer = new TextAnalyzerImpl();

        analyzer.analyze("12345");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(1, statistics.getWordsCount());
        assertEquals(5, statistics.getCharsCount());
        assertEquals(5, statistics.getCharsCountWithoutSpaces());
        assertEquals(0, statistics.getCharsCountOnlyPunctuations());

    }


    @Test
    public void analyzeMultiline() {
        TextAnalyzer analyzer = new TextAnalyzerImpl();

        analyzer.analyze("12345");
        analyzer.analyze("\n123, asd45");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(3, statistics.getWordsCount());
        assertEquals(16, statistics.getCharsCount());
        assertEquals(15, statistics.getCharsCountWithoutSpaces());
        assertEquals(1, statistics.getCharsCountOnlyPunctuations());
    }
}